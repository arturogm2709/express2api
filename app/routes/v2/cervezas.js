const express = require('express')
const router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController.js')

router.get('/', (req, res) => {
  // console.log('hemos pasado')
  cervezaController.index(req, res)
})
router.get('/search', (req, res) => {
  // console.log('hemos pasado')
  cervezaController.search(req, res)
})
router.get('/:id', (req, res) => {
  // console.log('hemos pasado')
  cervezaController.show(req, res)
})
router.post('/', (req, res) => {
  // console.log('hemos pasado')
  cervezaController.create(req, res)
})
router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
  // res.json({ mensaje: '¡Cerveza actualizada!' })
})
router.delete('/:id', (req, res) => {
  cervezaController.remove(req, res)
  // res.json({ mensaje: '¡Cerveza deleteada!' })
})

module.exports = router
