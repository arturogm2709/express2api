const Producto = require('../../models/v2/Producto')
const { ObjectId } = require('mongodb')

const index = (req, res) => {
  Producto.find((err, productos) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el producto'
      })
    }
    return res.json(productos)
  })
}

// const search = (req, res) => {
//   const q = req.query.q
//   Cerveza.find({ $text: { $search: q } }, (err, cervezas) => {
//     if (err) {
//       return res.status(500).json({
//         message: 'Error en la búsqueda'
//       })
//     }
//     if (!cervezas.length) {
//       return res.status(404).json({
//         message: 'No hemos encontrado cervezas que cumplan esa query'
//       })
//     } else {
//       return res.json(cervezas)
//     }
//   })
// }

const show = (req, res) => {
  const id = req.params.id
  // console.log('hola')
  Producto.findOne({ _id: id }, (err, producto) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener el producto'
      })
    }
    if (!producto) {
      return res.status(404).json({
        message: 'No tenemos este producto'
      })
    }
    return res.json(producto)
  })
}
const create = (req, res) => {
  const producto = new Producto(req.body)
  producto.save((err, producto) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el producto',
        error: err
      })
    }
    return res.status(201).json(producto)
  })
}
const update = (req, res) => {
  const id = req.params.id
  Producto.findOne({ _id: id }, (err, producto) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el producto',
        error: err
      })
    }
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (!producto) {
      return res.status(404).json({
        message: 'No hemos encontrado el producto'
      })
    }

    Object.assign(producto, req.body)

    producto.save((err, producto) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el producto'
        })
      }
      if (!producto) {
        return res.status(404).json({
          message: 'No hemos encontrado el productoa'
        })
      }
      return res.json(producto)
    })
  })
}
const remove = (req, res) => {
  const id = req.params.id

  Producto.findOneAndDelete({ _id: id }, (err, producto) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la producto'
      })
    }
    if (!producto) {
      return res.status(404).json({
        message: 'No hemos encontrado la producto'
      })
    }
    return res.json(producto)
  })
}

module.exports = {
  index,
  // search,
  show,
  create,
  update,
  remove
}
