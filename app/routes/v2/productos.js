const express = require('express')
const router = express.Router()
const productoController = require('../../controllers/v2/productoController.js')
const servicejwt = require('../../services/servicejwt')
const auth = require('../../middlewares/auth')

router.use(auth.auth)

// const moment = require('moment')

// const auth = function (req, res, next) {
//   console.log(req.headers.authorization)
//   if (!req.headers.authorization) {
//     return res.status(403).send({ message: 'No tienes permiso' })
//   }
//   const token = req.headers.authorization.split(' ')[1]
//   try {
//     payload = servicejwt.decodeToken(token)
//   } catch (error) {
//     return res.status(401).send(`${error}`)
//   }
//   next()
//   // res.status(200).send({ message: 'con permiso' })
// }

router.get('/', auth, (req, res) => {
  productController.index(req, res)
})
// router.get('/search', (req, res) => {
//   // console.log('hemos pasado')
//   cervezaController.search(req, res)
// })
router.get('/:id', (req, res) => {
  // console.log('hemos pasado')
  productoController.show(req, res)
})
router.post('/', (req, res) => {
  // console.log('hemos pasado')
  productoController.create(req, res)
})
router.put('/:id', (req, res) => {
  productoController.update(req, res)
  // res.json({ mensaje: '¡Cerveza actualizada!' })
})
router.delete('/:id', (req, res) => {
  productoController.remove(req, res)
  // res.json({ mensaje: '¡Cerveza deleteada!' })
})

// router.get('/', auth, productController.index})
module.exports = router
