const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductoSchema = new Schema({
  name: {
    type: String,
    required: true,
    maxlength: 20
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    maxlength: 255
  },
  create: {
    type: Date,
    default: Date.now
  }
})

const Producto = mongoose.model('Producto', ProductoSchema)

module.exports = Producto
