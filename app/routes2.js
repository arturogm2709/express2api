const express = require('express')
const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js')
const routerProductos = require('./routes/v2/productos.js')
const routerUsers = require('./routes/v2/users.js')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con MongoDB!' })
})
// PRUEBA AMBAR
// const Cerveza = require('./models/v2/Cerveza')

// router.get('/ambar', (req, res) => {
//   const miCerveza = new Cerveza({ nombre: 'Ambar' })
//   miCerveza.save((err, miCerveza) => {
//     if (err) return console.error(err)
//     console.log(`Guardada en bbdd ${miCerveza.nombre}`)
//   })
// })
router.use('/cervezas', routerCervezas)
router.use('/productos', routerProductos)
router.use('/users', routerUsers)
module.exports = router
