// para establecer las distintas rutas, necesitamos instanciar el express router
const express = require('express')
const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')

router.get('/', (req, res) => {
  // console.log('hemos pasado')
  cervezaController.index(req, res)
})
router.get('/', (req, res) => {
  // let page = req.query.page ?  req.query.page  : 1
  const page = req.query.page || 1
  res.json({ mensaje: `Lista de cervezas , página ${page}` })
})
router.get('/:id', (req, res) => {
  cervezaController.show(req, res)
  // res.json({ mensaje: `! A beber ${req.params.id} ¡` })
})
router.post('/', (req, res) => {
  cervezaController.store(req, res)
  // res.json({ mensaje: `Cerveza creada : ${req.body.nombre}` })
})
router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
  // res.json({ mensaje: '¡Cerveza deleteada!' })
})
router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
  // res.json({ mensaje: '¡Cerveza actualizada!' })
})

module.exports = router
