const connection = require('../config/dbconnection')
const Cerveza = require('../models/Cerveza.js')

const index = (req, res) => {
  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
  // console.log(cervezas)
  // res.json({ mensaje: `Lista de cervezas desde el controlador¡¡¡` })
}
// const find = (req, res) => {
//   const id = req.params.id
//   Cerveza.find(id, function (status, data, fields) {
//     res.status(status).json(data)
//   })
// }
const show = (req, res) => {
  const id = req.params.id
  Cerveza.find(id, function (status, data, fields) {
    res.status(status).json(data)

    // res.json({ mensaje: `! A beber ${req.params.id} ¡` })
    // res.json({ mensaje: `Lista de cervezas desde el controlador¡¡¡` })
  })
}
const store = (req, res) => {
  const cerveza = {
    name: req.body.name,
    description: req.body.description,
    alcohol: req.body.alcohol,
    container: req.body.container,
    price: req.body.price
  }
  // const name = req.body.name
  // const description = req.body.description
  // const alcohol = req.body.alcohol
  // const container = req.body.container
  // const price = req.body.price
  const cervezas = Cerveza.create(
    cerveza,
    // name,
    // description,
    // alcohol,
    // container,
    // price,
    (status, err, data) => {
      if (err) {
        res.status(status).json(err)
      } else {
        res.status(status).json(data)
      }
    }
  )
  // const id = req.params.id
  // const sql = 'SELECT * FROM cervezas where id =?'

  // res.json({ mensaje: `Cerveza creada : ${req.body.nombre}` })
  // res.json({ mensaje: `Lista de cervezas desde el controlador¡¡¡` })
}
const destroy = (req, res) => {
  const id = req.params.id
  Cerveza.destroy(id, function (status, data, fields) {
    res.status(status).json(data)
  })

  // res.json({ mensaje: `¡Cerveza ${req.params.id} deleteada` })
  // res.json({ mensaje: `Lista de cervezas desde el controlador¡¡¡` })
}
const update = (req, res) => {
  const id = req.params.id
  Cerveza.update(
    id,
    req.body.name,
    req.body.description,
    req.body.alcohol,
    req.body.container,
    req.body.price,
    function (status, data, fields) {
      res.status(status).json(data)
    }
  )
  // res.json({ mensaje: `¡Cerveza ${req.params.id} actualizada` })
  // res.json({ mensaje: `Lista de cervezas desde el controlador¡¡¡` })
}
module.exports = {
  index,
  show,
  store,
  destroy,
  update
}
