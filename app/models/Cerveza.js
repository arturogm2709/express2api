const connection = require('../config/dbconnection')

const index = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}
const find = function (id, callback) {
  // const id = req.params.id
  const sql = 'SELECT * FROM cervezas where id =?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}
const create = function (cerveza, callback) {
  const sql =
    'INSERT INTO cervezas (name,description,alcohol,container, price) ' +
    'VALUES(?,?,?,?,?)'

  connection.query(
    sql,
    [
      cerveza.name,
      cerveza.description,
      cerveza.alcohol,
      cerveza.container,
      cerveza.price
    ],
    (err, result) => {
      if (err) {
        console.log(err)
        callback(500, err)
      } else {
        console.log(result)
        callback(200, result)
      }
    }
  )
}
const destroy = function (id, callback) {
  const sql = 'DELETE  FROM cervezas where id =?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}
const update = function (
  id,
  name,
  description,
  alcohol,
  container,
  price,
  callback
) {
  const sql =
    'UPDATE cervezas SET name=?, description=?, alcohol=?,container=?, price=? where id=?'
  connection.query(
    sql,
    [name, description, alcohol, container, price, id],
    (err, result) => {
      if (err) {
        console.log(err)
        callback(500, err)
      } else {
        console.log(result)
        callback(200, result)
      }
    }
  )
}

module.exports = {
  index,
  find,
  create,
  destroy,
  update
}
