const express = require('express')
const router = express.Router()
const routerCervezas = require('./routes/cervezas.js')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API!' })
})

router.use('/cervezas', routerCervezas)
module.exports = router
